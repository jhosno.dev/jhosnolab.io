---
layout: post
title:  "RegExpr"
date:   2022-02-22 15:00:00 -0400
categories: [Reactjs, tutorial]
image: reactjs2.png
alt: "ReactJS, como hacer un CRUD | Crédito: CSS Generative Pattern Circles by crankysparrow at CodePen"
extract: "¿Quiéres hacer un CRUD en reactJS y no sabes como empezar? Aquí puedes hacerte una idea de como funciona la librería, a parte de usar tailwindsCSS . ¿Quieres saber más? Sigue leyendo"
---